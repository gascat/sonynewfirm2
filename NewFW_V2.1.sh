#!/bin/bash

mkdir ~/zentemp

zenity --info --text="Please download the firmware for the device you are flashing using Xperifirm"

# get model name
mod=$(zenity --entry --text="Please enter the device model name (e.g. XZ1)")

# create device directory on server
mkdir ~/Sony_share/$mod
nemo ~/Sony_share/$mod
zenity --info --text="Please copy the downloaded firmware folder to the model directory created in the Sony_share folder. Click OK once files have ben copied"


#create files
touch ~/zentemp/$mod.sh
touch ~/zentemp/""$mod"flash.sh"
touch ~/zentemp/""$mod"again.sh"

# echo bash lines
echo "#!/bin/bash" >> ~/zentemp/$mod.sh
echo "#!/bin/bash" >> ~/zentemp/""$mod"flash.sh"
echo "#!/bin/bash" >> ~/zentemp/""$mod"again.sh"

# get directory
dir=$(ls ~/Sony_share/$mod/)

# [model].sh
echo "cp -r ~/Sony_share/"$mod"/"$dir" ~/local" >> ~/zentemp/$mod.sh
echo "/bin/bash "$mod"flash.sh" >> ~/zentemp/$mod.sh
echo "/bin/bash "$mod"again.sh" >> ~/zentemp/$mod.sh

# [model]flash.sh
echo "cd ~/local/"$dir >> ~/zentemp/$mod"flash.sh"
echo 'newflasher.x64' >> ~/zentemp/$mod"flash.sh"

# [model]again.sh
echo 'zenity --question --text="Flash Again?" --ok-label="Yes" --cancel-label="No"' >> ~/zentemp/$mod"again.sh"
echo 'if [ $? = 0 ]; then' >> ~/zentemp/$mod"again.sh"
echo '	command=$(/bin/bash ~/Sony_Flasher/models/'$mod"flash.sh"" &)" >> ~/zentemp/$mod"again.sh"
echo 'else' >> ~/zentemp/$mod"again.sh"
echo '	command=$(rm -r ~/local/'$mod"/"$dir")" >> ~/zentemp/$mod"again.sh"
echo '	zenity --error --text="please close the window"' >> ~/zentemp/$mod"again.sh"
echo 'fi' >> ~/zentemp/$mod"again.sh"

# copy new scripts to correct locations

chmod 777 ~/zentemp/*

cp -r ~/zentemp/$mod.sh ~/Sony_Flasher/models
cp -r ~/zentemp/$mod"flash.sh" ~/Sony_Flasher/models
cp -r ~/zentemp/$mod"again.sh" ~/Sony_Flasher/models

chmod 777 ~/Sony_Flasher/models/*

#create 2nd set of scripts fot copying to other machines

mkdir /root/New_Sony_Scripts

chmod 777 ~/zentemp/*

cp -r ~/zentemp/${mod}.sh ~/New_Sony_Scripts
cp -r ~/zentemp/${mod}"flash.sh" ~/New_Sony_Scripts
cp -r ~/zentemp/${mod}"again.sh" ~/New_Sony_Scripts

zenity --info --text="Please copy the files created in /root/New_Sony_Scripts to /root/Sony_Flasher/local on other machines"

# set permissions
chmod 777 ~/Sony_Flasher/local/*

# add menu entry
# deprecated menu now auto generated


# Remove unwanted files from firmware folder

rm -r ~/Sony_share/$dir/simlock.ta

#cleanup temp files 
rm -r ~/zentemp/*
rm ~/zentemp
